Nest module that supports VPN service.

Commands
-----------------------------------

Manually generate a client certificate + key bundle:

    exe/generate-client-certificate --ca-cert ./client_ca.crt --ca-key ./client_ca.key > cert.pem

Where client_ca.crt and client_ca.key are the cert and key for the Certicate Authority that will issue the client certificates.

To inspect cert.pem:

   openssl x509 -text -noout -in cert

